set -ex

export BOARD=veyron_jaq
export PATH=$PATH:/chromeos/chromite/bin/

pushd /chromeos
cros_sdk --nouse-image -- ./build_packages --board=${BOARD} --nowithautotest --noworkon
cros_sdk --nouse-image -- ./build_image --board=${BOARD} --noenable_rootfs_verification --enable_serial=ttyS2 test
find ../build/images/
popd
