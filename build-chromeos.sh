set +ex

export BOARD=amd64-generic
export PATH=$PATH:/chromeos/chromite/bin/

pushd /chromeos
cros_sdk --nouse-image -- ./build_packages --board=${BOARD}
cros_sdk --nouse-image -- ./build_image --board=${BOARD}
popd
