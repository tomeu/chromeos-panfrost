#!/bin/bash

set -e
set -o xtrace

export DEBIAN_FRONTEND=noninteractive

apt-get install -y ca-certificates

echo 'deb http://ftp.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list

apt-get update

apt-get dist-upgrade -y

apt-get install -y --no-remove \
    git-core \
    gitk \
    git-gui \
    subversion \
    curl \
    lvm2 \
    thin-provisioning-tools \
    python-pkg-resources \
    python-virtualenv \
    ccache \
    python3-oauth2client \
    xz-utils \
    sudo

curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
chmod a+x /usr/local/bin/repo

git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git

useradd -m cros-builder
mkdir -p /chromeos /ccache
chown cros-builder:cros-builder /chromeos /ccache

AS_USER="sudo -u cros-builder BOARD=veyron_jaq PATH=$PATH:$(pwd)/depot_tools:/chromeos/chromite/bin/ USE_CCACHE=1 CCACHE_DIR=/ccache"

$AS_USER git config --global user.name 'ChromeOS Build'
$AS_USER git config --global user.email 'foo@example.com'
$AS_USER git config --global color.ui true

pushd /chromeos
$AS_USER repo init --depth 1 -u https://gitlab.collabora.com/tomeu/chromiumos_manifest.git
$AS_USER repo sync --current-branch --force-sync --no-clone-bundle --no-tags -j5
du -sh .
ls -l /chromeos/chromite/bin/cros_sdk
/chromeos/chromite/bin/cros_sdk --nouse-image -- ./setup_board --board=${BOARD}
#$AS_USER cros_sdk -- ./build_packages --board=${BOARD}
#$AS_USER cros_sdk -- ./build_image --board=${BOARD} --noenable_rootfs_verification --enable_serial=ttyS2 test
popd

du -sh /chromeos

