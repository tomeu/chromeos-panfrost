#!/bin/bash

set -e
set -o xtrace

export DEBIAN_FRONTEND=noninteractive

apt-get install -y ca-certificates

echo 'deb http://ftp.debian.org/debian experimental main' > /etc/apt/sources.list.d/experimental.list

apt-get update

apt-get dist-upgrade -y

apt-get install -y --no-remove \
    git-core \
    gitk \
    git-gui \
    subversion \
    curl \
    lvm2 \
    thin-provisioning-tools \
    python-pkg-resources \
    python-virtualenv \
    ccache \
    python3-oauth2client \
    xz-utils \
    sudo

curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
chmod a+x /usr/local/bin/repo

git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git

useradd -m cros-builder
mkdir -p /chromeos /ccache
chown cros-builder:cros-builder /chromeos /ccache

AS_USER="sudo -u cros-builder BOARD=amd64-generic PATH=$PATH:$(pwd)/depot_tools USE_CCACHE=1 CCACHE_DIR=/ccache"

$AS_USER git config --global user.name 'ChromeOS Build'
$AS_USER git config --global user.email 'foo@example.com'
$AS_USER git config --global color.ui true

pushd /chromeos
$AS_USER repo init -u https://chromium.googlesource.com/chromiumos/manifest.git
#pushd .repo
#git clone https://github.com/robherring/android_manifest.git # -b ${CI_COMMIT_REF_NAME}
#popd
$AS_USER repo sync -q -f --force-sync --no-clone-bundle --no-tags -j10
#$AS_USER cros_sdk -- ./build_packages --board=${BOARD}
#$AS_USER cros_sdk -- ./build_image --board=${BOARD}
popd

du -sh /chromeos

